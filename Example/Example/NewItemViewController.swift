//
//  NewItemViewController.swift
//  Example
//
//  Created by Marina Georgijevic on 5/20/18.
//  Copyright © 2018 Marina Georgijevic. All rights reserved.
//

import UIKit

class NewItemViewController: UIViewController, UITextFieldDelegate{
    
    @IBOutlet var textInput: UITextField!
    @IBOutlet var alertMessage: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textInput.returnKeyType = UIReturnKeyType.done
    }
    
    @IBAction func doneBtnTapped(_ sender: UIButton) {
        guard textInput.text != "" else {
            alertMessage.text = "Please insert some device."
            alertMessage.isHidden = false
            return
        }
        
        guard GlobalVariables.tableArray.contains(textInput.text!) else {
            GlobalVariables.tableArray.append(textInput.text!)
            alertMessage.text = textInput.text! + " successfully added!"
            alertMessage.isHidden = false
            textInput.text = ""
            return
        }
        
        alertMessage.text = textInput.text! + " is already added."
        alertMessage.isHidden = false
        textInput.text = ""
    }
    
    @IBAction func cancelBtnTapped(_ sender: UIButton) {
        textInput.text = ""
    }
    
    
    // MARK: textFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        alertMessage.isHidden = true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        print(textInput.text as Any)
    }
    
}

