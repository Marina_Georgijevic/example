//
//  ViewController.swift
//  Example
//
//  Created by Marina Georgijevic on 5/20/18.
//  Copyright © 2018 Marina Georgijevic. All rights reserved.
//

import UIKit

struct GlobalVariables {
    static var tableArray = [String]()
}

class TableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    @IBOutlet var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()

        let path = Bundle.main.path(forResource: "TableData", ofType: "plist")
        let dict = NSDictionary(contentsOfFile:path!)
        
        if (dict != nil) {
            GlobalVariables.tableArray = dict?.object(forKey: "Devices") as! [String]
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        tableView.reloadData()
    }

    // MARK: tableView dataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return GlobalVariables.tableArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellId", for: indexPath) as! TableCell
        cell.textLabel?.text = GlobalVariables.tableArray[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            GlobalVariables.tableArray.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .automatic)
        }
    }

}

