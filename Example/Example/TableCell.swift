//
//  TableCell.swift
//  Example
//
//  Created by Marina Georgijevic on 5/20/18.
//  Copyright © 2018 Marina Georgijevic. All rights reserved.
//

import UIKit

class TableCell: UITableViewCell {
    
    @IBOutlet var titleLabel: UILabel!
    
}
